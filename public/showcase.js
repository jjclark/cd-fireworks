// Use JQuery's onload event to run code when the page has loaded 
$(function(){

	var projects=[];

	function loadProjects(){
		$.getJSON('./projects.json', function(data){
			projects = data;
			setProject(0);
			populateDropDown();
		});
	}

	function populateDropDown(){
		$('#selector').change(function(){
			updateNav(this.selectedIndex);
		});
		projects.forEach(function(d){
			$('#selector').append('<option>'+d.name+'</option>');
		})
	}

	function hookEvents(){
		var clickHandler=function(){
			setProject(parseInt(this.getAttribute('index')));
		};
		$('.button.prev').click(clickHandler);
		$('.button.next').click(clickHandler);
	}

	function updateNav(index) {
		var prev = projects[index-1];
		var next = projects[index+1];

		if(prev) {
			$('#prevName').text(prev.name);
			$('#prevName').parent().attr("index", index-1);
		}else {
			$('#prevName').text("[none]");
			$('#prevName').parent().attr("index", -1);
		}

		if(next) {
			$('#nextName').text(next.name);
			$('#nextName').parent().attr("index", index+1);
		}else {
			$('#nextName').text("[none]");
			$('#nextName').parent().attr("index", -1);
		}
	}

	function setProject(index) {
		var project = projects[index];
		if(project) {
			// Gather the nodes we need
			var creatorNode = $('#creator');
			var titleNode = $('#title');
			var scratchFrame = $('#scratch');

			// Build a URL to the scratch project
			var url = "//scratch.mit.edu/projects/embed/"+project.projectId+"/?autostart=true"

			console.log(url);

			// set the iframe's location
			scratchFrame.attr("src", url);
			creatorNode.text(project.name);
			titleNode.text(project.title||"Fireworks");
			
			var projecturl = "//scratch.mit.edu/projects/"+project.projectId+'#editor';
			$('#project-link').attr('href', projecturl);
		}
		updateNav(index);
	}

	hookEvents();
	loadProjects();
})