// Use JQuery's onload event to run code when the page has loaded 
$(function(){

	var projects=[];

	function loadProjects(){
		$('.button.add').click(function(){
			add();
		})
		$.getJSON('./projects.json', function(data){
			projects = data;
			data.forEach(render);
		});
	}

	function add(){
		var item = {};
		projects.push(item);
		render(item)
	}

	function render(item) {
		var table = $('#projectBody');
		
		var row = document.createElement('tr');
		createCell(row, 'name', item.name, function(){
			item.name=this.value;
			save();
		});
		createCell(row, 'project-id', item.projectId, function(){
			item.projectId=this.value;
			save();
		});
		createCell(row, 'title', item.title, function(){
			item.title=this.value;
			save();
		});
		var del = document.createElement('td');
		del.className = "delete";
		del.textContent="x";
		del.onclick=function(){
			for(var i=0;i<projects.length;i++){
				if(projects[i].projectId==item.projectId && projects[i].name==item.name ) {
					break;
				}
			}
			projects.splice(i,1);
			save();
			this.parentNode.parentNode.removeChild(this.parentNode);
		}
		row.appendChild(del)

		table.append(row);
	}

	function createCell(parent, className, value, handler) {
		var root = document.createElement('td');
		root.className="field"
		if(className) {
			root.className+=" "+className;
		}
		var input = document.createElement('input');
		input.value=value||"";
		input.onchange=handler;
		root.appendChild(input);

		parent.appendChild(root)
	}

	function save(){
		// post to server
		var data = JSON.stringify(projects, null, 4);
		console.log(data);


		$.post({
			url: '/update',
			contentType: 'application/json',
			data: data
		}).then(function(err, result){
				console.log("Save ok!!");
			});
	}

	loadProjects();
})