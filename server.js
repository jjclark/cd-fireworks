var fs = require('fs');
var express = require('express');
var bodyParser = require('body-parser');
var app = express();

app.use(express.static('public', { extensions:['html'] }));

app.use(bodyParser.json());

app.post('/update', function(req, res){
	console.log(req.body);
	if(req.body && !isNaN(req.body.length)) {
		fs.writeFile('./public/projects.json', JSON.stringify(req.body,null,4), function(err){
			if(err) {
				console.log(" ** Error writing persistent file **");
				res.sendStatus(500)
			}else {
				console.log(" >> data updated");
				res.sendStatus(200)
			}
		});
	}else {
		console.log(" ** Error reading post data **");
		return res.sendStatus(500)
	}
})

app.listen(process.env.PORT || 8080, function(){
	console.log("Simple web server :: running on "+8080);
})